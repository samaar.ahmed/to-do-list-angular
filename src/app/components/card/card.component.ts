import { HttpClient } from '@angular/common/http';
import { Component,Input, OnInit } from '@angular/core';
import { TodoList } from 'src/app/todolist';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  newTodoArr : TodoList[];
  mytodoList !:TodoList[];
  

  constructor(private http:HttpClient){}
  ngOnInit(){
    this.fetchTodo()
  }
  
  fetchTodo(){
    this.http.get<TodoList[]>("https://jsonplaceholder.typicode.com/todos")
    .subscribe((res : TodoList[])=>{
      this.mytodoList = res.filter(n=>n.userId === 1 && n.completed === false)
      console.log(this.mytodoList)
    })
  }


  input(event:string){
    let todo = new TodoList(1,1,event,false);
    this.mytodoList.push(todo);

    //this.newTodoArr = this.addNewToDo(this.mytodoList, todo)
    console.log(this.mytodoList)
  }

  addNewToDo<T> (arr:T[], item:T){
    const updataTodoArray = [...arr,item];
    console.log(updataTodoArray)
    return updataTodoArray
  }
  newTodo : string;


  /*savenewTodo(){

  let todo = new TodoList("hello",true);
  todo.name = this.newTodo;
  todo.isCompleted= true
  this.mytodoList.push(todo);
  this.newTodo = "";

  }*/


}
