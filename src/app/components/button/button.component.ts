import { Component, EventEmitter, Output,Input } from '@angular/core';
import { TodoList } from 'src/app/todolist';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent {

@Input() newtodo !: HTMLInputElement
@Output() btnClicked = new EventEmitter<string>()
  



onClick(){

    this.btnClicked.emit(this.newtodo.value)
    console.log(this.newtodo.value)
  }
}
